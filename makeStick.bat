@echo off

set options_pdflatex="-max-print-line=120 -interaction=nonstopmode -c-style-errors"
set pdfltx=..\Programme\MikTex\miktex\bin\pdflatex.exe
set bbr=..\Programme\MikTex\miktex\bin\biber.exe
::hier Dateiname ohne Endung einstellen
set file= "main"


rem -- make PDF (three Times with bibtex) ----------------------------------------------

%pdfltx% %options_pdflatex% %file%.tex 
%bbr% %file%
%pdfltx% %options_pdflatex% %file%.tex 
%pdfltx% %options_pdflatex% %file%.tex 



::kopiert die Datei unter dem Aktuellem Versionsnamen ab
::funktioniert nur wenn die hooks verwendet werden!!

set /p describe=<.git\filename
@echo d|xcopy %file%".pdf" %describe%".pdf" /Y

::----------------------------------------------------------
pause
goto:eof
