@echo off


set options_pdflatex="-max-print-line=120 -interaction=nonstopmode -c-style-errors"

::hier Dateiname ohne Endung einstellen
set file= "main"


rem -- make PDF (three Times with bibtex) ----------------------------------------------
pdflatex %options_pdflatex% %file%.tex
biber %file%
pdflatex %options_pdflatex% %file%.tex
pdflatex %options_pdflatex% %file%.tex

::kopiert die Datei unter dem Aktuellem Versionsnamen ab
::funktioniert nur wenn die hooks verwendet werden!!

set /p describe=<.git\filename
@echo d|xcopy %file%".pdf" %describe%".pdf" /Y

::----------------------------------------------------------
pause
goto:eof
