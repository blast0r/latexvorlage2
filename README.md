# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

## Git-Info-Hooks ##
am Anfang müssen einmal die Hooks im lokalen Repository gesettzt werden. Dazu einfach per "cd hooks" in den hooks ordner wechseln und copyHooks aufrufen.
Ab jetzt werden dann die Hooks auch ausgeführt und die Version im PDF dokument angezeigt.

## Bibliography ##
Ich habe das Literaturverzeichnis direkt mit JabRef editiert und bin keine Umwege über Citavi oder so gegangen. 
Das hat auch ganz gut funktioniert.
Man muss nur in JabRef etwas umstellen:

* Biber als Backend
* UTF8 als Dateicodierung!! (das ist in dieser Vorlage schon so eingestellt, JabRef wird es automatisch als UTF8 öffnen)

Es kann vorkommen, dass Git bei Diffs so ^M anzeigt.. das ist ein windows-Zeilenendungsproblem.. das musste man glaub ich einmal lösen oder so


## TeXnicCenter mit Sumatra PDF ##
Vorteil: Springt beim Compilen direkt an die Richtige STelle und Inverse-Search möglich.

Einstellungen der Profile

* Latex-Compiler in TeXnicCenter, Argumente: zusätzlich "-synctex=-1" als erstes!


## TeXnicCenter mit Biber als Backend ##
Ich habe einfach in dne Profilen unter den BibTex einstellungen den Pfad auf "biber.exe" zeigen lassen..
Ich schiebe TC damit einfach biber als backend unter...das hat bisher ganz gut funktioniert.